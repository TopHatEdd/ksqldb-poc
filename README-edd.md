# Requirements
* Write `AVRO` data to two topiocs.
* Use `Kafka Streams` and `ksqlDB`.
* One stream per topic.
* Third stream that'll ingest the above two and perform some transformation.

# POC
## Docs and materials used
* [ksqlDB git](https://github.com/confluentinc/ksql)
* [ksqlDB quickstart](https://ksqldb.io/quickstart.html)
* [create stream](https://docs.ksqldb.io/en/latest/developer-guide/ksqldb-reference/create-stream/)
* [join stream](https://docs.ksqldb.io/en/latest/developer-guide/joins/join-streams-and-tables/)
* [terminate query](https://docs.ksqldb.io/en/latest/developer-guide/ksqldb-reference/terminate/)
* [kafka schema API](https://docs.confluent.io/platform/current/schema-registry/develop/api.html#schemaregistry-api)
* [kafka python docs](https://github.com/confluentinc/confluent-kafka-python)
* [python avro blog](https://medium.com/better-programming/avro-producer-with-python-and-confluent-kafka-library-4a1a2ed91a24)

## Deploying infra
We'll start up the infrastructure using
```bash
docker-compose up -d
```
we'll have a simple `Kafka`, `Zookeeper` and `ksqlDB` deployment running.

## Writing to topics (manually)
### Build
```bash
docker build -t allot/kafka-producer:0.1 python-producer
```
### Write
We'll write to two topics, `conv` and `sdr`. We'll use `Python` to write
`AVRO` structured data into said topics. There's an ugly ugly ugly piece of
`Python` code (*do not* use any of it in production) I glued from the docs
and some blog under the dir `python-producer`.
```bash
docker run --rm --network ksql_default allot/kafka-producer:0.1 python /conv.py --topic conv --trash-value unknown --bootstrap-servers kafka:29092 --schema-registry 'http://schema-registry:8081'
docker run --rm --network ksql_default allot/kafka-producer:0.1 python /conv.py --topic sdr --trash-value no --bootstrap-servers kafka:29092 --schema-registry 'http://schema-registry:8081'
```
The code writes `AVRO` of two fields into both topics. One field is called 
`sub_id` and the other is `trash`. The "business logic" is that the `trash`
field decided wether or not the row should be discarded. This information
comes only from `sdr`.
The code writes a hard-coded subscriber_id value. And, through the 
`--trash-value` flag, we decide the value of `trash`. The other flags are 
self-explanatory.

### Troubleshooting
#### List topics
```bash
docker-compose exec kafka kafka-topics --zookeeper zookeeper:32181 --list
```

#### Delete topic
```bash
docker-compose exec kafka kafka-topics --zookeeper zookeeper:32181 --delete --topic test
```

#### Read topic data
```bash
docker-compose exec kafka kafka-console-consumer --bootstrap-server kafka:29092 --topic conv --from-beginning
```

## Kafka Streams
We'll create both 1:1 streams for `conv` and `sdr`.
```bash
echo "create stream conv (sub_id VARCHAR, trash VARCHAR) with (kafka_topic='conv', value_format='avro');" | docker-compose exec -T ksqldb-cli ksql http://primary-ksqldb-server:8088
echo "create stream sdr (sub_id VARCHAR, trash VARCHAR) with (kafka_topic='sdr', value_format='avro');" | docker-compose exec -T ksqldb-cli ksql http://primary-ksqldb-server:8088
```
and the enrichment stream.
```bash
echo "create stream rich_conv as select conv.sub_id as sub_id, LEN(conv.sub_id) AS some_value from conv inner join sdr within 1 hours on conv.sub_id=sdr.sub_id where sdr.trash <> 'yes' emit changes;" | docker-compose exec -T ksqldb-cli ksql http://primary-ksqldb-server:8088
```

### Troubleshooting
#### List stream
```bash
echo "show streams;" | docker-compose exec -T ksqldb-cli ksql http://primary-ksqldb-server:8088
```
#### Delete stream
```bash
echo 'drop stream conv;' | docker-compose exec -T ksqldb-cli ksql http://primary-ksqldb-server:8088
# If a stream errors about an active query on deletion, we must terminate the query.
echo 'terminate CSAS_RICH_CONV_17;' | docker-compose exec -T ksqldb-cli ksql http://primary-ksqldb-server:8088
```
#### Read stream data
```bash
echo 'select * from rich emit changes;' | docker-compose exec -T ksqldb-cli ksql http://primary-ksqldb-server:8088
```

