import uuid
from argparse import ArgumentParser
from confluent_kafka import avro
from confluent_kafka.avro import AvroProducer


value_schema_str = """
{
   "namespace": "my.test",
   "name": "value",
   "type": "record",
   "fields" : [
     {
       "name" : "sub_id",
       "type" : "string"
     },
     {
       "name" : "trash",
       "type" : "string"
     }
   ]
}
"""

key_schema_str = """
{
   "namespace": "my.test",
   "name": "key",
   "type": "record",
   "fields" : [
     {
       "name" : "name",
       "type" : "string"
     }
   ]
}
"""


def parse_command_line_args():
    arg_parser = ArgumentParser()

    arg_parser.add_argument("--topic", required=True, help="Topic name")
    arg_parser.add_argument("--bootstrap-servers", required=True, help="Bootstrap server address")
    arg_parser.add_argument("--schema-registry", required=True, help="Schema Registry url")
    # arg_parser.add_argument("--schema-file", required=True, help="File name of Avro schema to use")
    # arg_parser.add_argument("--record-key", required=False, type=str, help="Record key. If not provided, will be a random UUID")
    arg_parser.add_argument("--trash-value", required=True, help="Trash value")

    return arg_parser.parse_args()


def delivery_report(err, msg):
    """ Called once for each message produced to indicate delivery result.
        Triggered by poll() or flush(). """
    if err is not None:
        print('Message delivery failed: {}'.format(err))
    else:
        print('Message delivered to {} [{}]'.format(msg.topic(), msg.partition()))


def send_record(args):
    print(args)
    value_schema = avro.loads(value_schema_str)
    key_schema = avro.loads(key_schema_str)
    value = {"sub_id": "Value", "trash": args.trash_value}
    key = {"name": str(uuid.uuid4())}

    producer = AvroProducer({
            'bootstrap.servers': args.bootstrap_servers,
            'on_delivery': delivery_report,
            'schema.registry.url': args.schema_registry},
        default_key_schema=key_schema,
        default_value_schema=value_schema)

    try:
        producer.produce(topic=args.topic, key=key, value=value)
    except Exception as e:
        print(f"Exception while producing record value - {value} to topic - {args.topic}: {e}")
    else:
        print(f"Successfully producing record value - {value} to topic - {args.topic}")

    producer.flush()


if __name__ == "__main__":
    send_record(parse_command_line_args())
